function get_user_data(req){
   var user_data = {
        image : req.session.data.image,
        email : req.session.data.email
    }
    //setze username auf den namen oder falls leer auf die email
    if (req.session.data.user_name == ""){
      user_data.name = req.session.data.email;
    } else {
      user_data.name = req.session.data.user_name;
    }
    //checken ob Admin
    if (req.session.data.rang == "admin"){
        user_data.admin = true;

    }
    return user_data;
}

module.exports = {
    get_user_data
};
