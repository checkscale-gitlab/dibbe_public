//Image Funktionalitäten für die google Cloud

'use strict';
//Multer zur Zwichenspeicherung des BIldes
const Multer = require('multer');
const multer = Multer({
  storage: Multer.MemoryStorage,
  limits: {
    fileSize: 5 * 1024 * 1024 // no larger than 5mb
  }
});

// Configs für die Google CLoud
const Storage  = require('@google-cloud/storage');
const projectId = 'foodenw';
const storage = new Storage({
    keyFilename: 'foodenw-03396afa3062.json',
    projectId: projectId
});

//var bucket = storage.bucket("lebensmittel_img");


function getPublicUrl (filename, url) {
  if (url == "/api/image"){
    return 'https://storage.googleapis.com/lebensmittel_img/'+filename;
  } else {
    return 'https://storage.googleapis.com/dibbe_profile_img/'+filename;
  }
};
//upload funktion zur google cloud
function upload(req, res, next){
  var bucket;
  if (req.originalUrl == "/api/image"){
    bucket = storage.bucket("lebensmittel_img");
  } else if (req.originalUrl == "/api/profile_img"){
    bucket = storage.bucket("dibbe_profile_img");
  } else {
    return next();
  }
  if (!req.file) {
    return next();
  }
  const gcsname = Date.now() + "_" + req.file.originalname;
  const file = bucket.file(gcsname);
  const stream = file.createWriteStream({
        metadata: {
            contentType: req.file.mimetype
        }
  });

  stream.on('error', (err) => {
        req.file.cloudStorageError = err;
        next(err);
  });

  stream.on('finish', () => {
        req.file.cloudStorageObject = gcsname;
        file.makePublic().then(() => {
            req.file.cloudStoragePublicUrl = getPublicUrl(gcsname, req.originalUrl);
            next();
        });
  });
  stream.end(req.file.buffer);
};


module.exports = {
    getPublicUrl,
    upload,
    multer
};
