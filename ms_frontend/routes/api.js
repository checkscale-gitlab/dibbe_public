var express = require('express');
var router = express.Router();
var request = require('request-promise');
var config = require('config');
//Getconfig
var backendip = config.get('ip');
var main_uri = backendip+'/lebensmittel';
var options = {
    json: true,
    insecure: true,
    rejectUnauthorized: false
};

//einbindung des Bilder Uploads
const image = require('../lib/image');

//APIS
//Frontend to Backend routen

router.get('/produkt_list', function(req, res, next) {
    if (req.query.data === 'new') {
        //Nur neue Produkte laden
        options.uri = main_uri + "?data=new";
    } else {
        //Gesamte Produkt-Liste laden
        options.uri = main_uri;
    }
    options.method = 'GET';
    console.log(options.uri);
    request(options).then(data => {
          console.log("OK: msldata ausgelesen.");
          res.send(data);
    }).catch(err => {
          console.log("ERROR: msldata ausgelesen.");
          res.send("fail");
    });
});

//get produkt
router.get('/:produkt_id', function(req, res, next) {
    //uri setzen fuer Backend
    options.uri = main_uri + '/' + req.params.produkt_id;
    options.method = 'GET';
    console.log(options.uri);
    request(options).then(produkt => {
          console.log("OK: Produkt: "+req.params.produkt_id+" ausgelesen.");
          res.send(produkt);
    }).catch(err => {
          console.log("ERROR: Fehler beim Auslesen des Produkts: "+req.params.produkt_id);
          res.send("fail");
    });
});
router.post('/save', function(req, res, next) {
    console.log(req.body);
    //umformatieren der Data
    var data = {
        "name" : req.body.name,
        "barcode" : {
            "code": req.body.code,
            "typ": req.body.typ
        }
    }
    console.log(data);
    options.uri = main_uri;
    options.method = 'POST';
    options.body = data;
    console.log(options.uri);
    request(options).then(result => {
          console.log("OK: Produkt: "+req.body.name+" wurde erfolgreich gespeichert!.");
          res.send("ok");
    }).catch(err => {
          console.log("ERROR: Fehler beim Speichern des Produkts: "+req.body.name);
          res.send(err.name);
    });
});

router.patch('/save', function(req, res, next) {
    //Formatieren des Arrays
    var patch_data = {
        "_id" : req.body._id,
        "name" : req.body.name,
        "barcode" : {
            "code": req.body.code,
            "typ": req.body.typ
        },
        "naehrwerte" : {
            "kcal": req.body.kcal,
            "eiweiß": req.body.eiweiß,
            "kohlenhydrate" : {
                "k_anzahl" : req.body.k_anzahl,
                "zucker" : req.body.zucker
            },
            "fett" : {
                "f_anzahl" : req.body.f_anzahl,
                "gesaetiigt" : req.body.gesaetiigt
            },
            "ballaststoffe" : req.body.ballaststoffe,
            "natrium" : req.body.natrium,
            "salz" : req.body.salz
        },
        "verderblich" : req.body.verderblich,
        "erstellt" : req.body.erstellt
    }
    options.uri = main_uri + '/' + req.body._id;
    options.method = 'PATCH';
    options.body = patch_data;
    console.log(options.uri);
    request(options).then(result => {
          console.log("OK: Produkt: "+req.body._id+" wurde erfolgreich gepatcht!.");
          res.send("ok");
    }).catch(err => {
          console.log("ERROR: Fehler beim Patchen des Produkts: "+req.body._id);
          res.send("fail");
    });
});

//Multer zur Zwichenspeicherung des BIldes
const Multer = require('multer');
const multer = Multer({
  storage: Multer.MemoryStorage,
  limits: {
    fileSize: 5 * 1024 * 1024 // no larger than 5mb
  }
});

router.post('/image', image.multer.single('image'), image.upload, function(req, res, next) {
    console.log(req.file);
    console.log(req.body);
  if (req.file && req.file.cloudStoragePublicUrl) {
      //Bild erfolgreich gespeichert
      var patch_data = {
          "image" : req.file.cloudStoragePublicUrl
      };
      //MongoDB updaten
        options.uri = main_uri + '/' + req.body._id;
        options.method = 'PATCH';
        options.body = patch_data;
        console.log(options.uri);
        request(options).then(result => {
          console.log("OK: Das Bild für das Produkt: "+req.body._id+ "wurde erfolgreich gespeichert!");
          res.send("ok");
        }).catch(err => {
          console.log("ERROR: Fehler beim Speichern des Bildes des Produkts: "+req.body._id);
          res.send("fail");
        });
  } else {
        console.log("ERROR: Fehler beim Speichern des Bildes des Produkts:");
        res.send("fail");
  }
});


//Login USER MIT NORMALEM PW
router.post('/login', function(req, res, next) {
  if (req.body.password != ""){
    options.uri = config.get("msluser")+"/login";
    options.method = 'POST';
    options.body = req.body;
    console.log(options.uri);
    request(options).then(result => {
          if (result != "fail"){
              console.log("OK: User erfolgreich eingeloggt!.");
              req.session.loggin = true;
              req.session.data = result;
              res.redirect('/');
          } else {
              console.log("ERROR: Fehler beim einloggen des Users"+req.body.email);
              req.session.loggin = false;
              res.redirect('/login?login=false');
          }
    }).catch(err => {
          console.log("ERROR: Fehler beim einloggen des Users"+req.body.email);
          req.session.loggin = false;
          res.redirect('/login?login=false');
    });
  } else {
    req.session.loggin = false;
    res.redirect('/login?login=false');
  }
});

//login normal Registrieren
router.post('/reg', function(req, res, next) {
  options.uri = config.get("msluser");
  options.method = 'POST';
  options.body = req.body;
  console.log(options.uri);
  request(options).then(result => {
        if (result != "fail"){
            console.log("OK: User erfolgreich registriert.");
            req.session.loggin = true;
            req.session.data = {
              "user_name" : req.body.email,
              "rang" : "user"
            };
            res.redirect('/');
        } else {
            console.log("ERROR: Fehler bei der Registration");
            req.session.loggin = false;
            res.redirect('/login?reg_page=true&reg=false');
        }
  }).catch(err => {
        console.log("ERROR: Fehler bei der Registration");
        req.session.loggin = false;
        res.redirect('/login?reg_page=true&reg=false');
  });
});


router.post('/profile_img', image.multer.single('image'), image.upload, function(req, res, next) {
  if (req.file && req.file.cloudStoragePublicUrl) {
      //Bild erfolgreich gespeichert
    options.uri = config.get("msluser")+"/profil/"+req.session.data.email;
    options.method = 'PATCH';
    options.body = {"image" : req.file.cloudStoragePublicUrl};
    request(options).then(result => {
          if (result != "fail"){
              console.log("OK: Profilbild erfolgreich aktualisiert!");
              req.session.image = req.file.cloudStoragePublicUrl;
              res.send(req.session.image);
          } else {
              console.log("ERROR: Fehler Bilder upload!");
              res.send("fail");
          }
    }).catch(err => {
      console.log("ERROR: Fehler beim aktualisieren des Profilbilds!");
      res.send("fail");
    });
  } else {
    console.log("ERROR: Fehler beim aktualisieren des Profilbilds!");
    res.send("fail");
  }
});
module.exports = router;
