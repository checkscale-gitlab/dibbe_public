var express = require('express');
var router = express.Router();
var request = require('request-promise');
var config = require('config');

router.get('/', function(req, res, next) {
    var err = "";
    if (req.query.login=='false'){
        err = "Email Adresse oder Passwort falsch!";
    };
    if (req.query.google=='false'){
        err = "Fehler bim einloggen mit Google!";
    };
    if (req.query.logout=='true'){
        err = "Erfolgreich ausgeloggt";
    };
    if (req.query.reg == "false"){
        err = "Fehler bei der Registrierung!";
    }
    res.render('login', {err : err, pagetitle : "Log In", login : true});

});

module.exports = router;
