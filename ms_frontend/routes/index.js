var express = require('express');
var router = express.Router();
var request = require('request-promise');
var config = require('config');
var user_data = require('../lib/user_data');

//Getconfig
var backendip = config.get('ip');
var main_uri = backendip+'/lebensmittel';
var options = {
    json: true,
    insecure: true,
    rejectUnauthorized: false
};

router.get('/', function(req, res, next) {
    console.log(req.session.data);
    res.render('index', {pagetitle : "Lager", user_data : user_data.get_user_data(req)});
});

module.exports = router;
