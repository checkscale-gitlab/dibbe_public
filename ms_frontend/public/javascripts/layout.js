//profil
function show_childs(v){
    if($(v).css("display") == 'none'){
        $(v).css("display", "block");       
    } else {
        $(v).css("display", "none");    
    }
}

//logout
function logout(){
    window.location = "/logout";
};

//Admin
//Produktdatenbank
function produktdatenbank(){
    window.location = "/admin/data";
}
function produktscan(){
    window.location = "/admin/scan";
}

//slideout menu
var slideout = new Slideout({
        'panel': document.getElementById('panel'),
        'menu': document.getElementById('menu'),
        'padding': 256,
        'tolerance': 70,
        'easing': 'linear'
});

// Toggle button
document.querySelector('.toggle-button').addEventListener('click', function() {
    slideout.toggle();
});

