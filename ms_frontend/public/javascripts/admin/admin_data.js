//Eingebundene JS:
//login : ist für den logout button
//filters : stellt funktionen zur verfügung um filter auszulesen etc.

//initale funktion
//checked Filter
$(document).ready(function(){
    if (filter_is_in("data", "new")){
        $('#filter_list_new').prop("checked",true);
    }
});


// Diese Funktion wir durch klick auf den Refresh-Button
// oder der Auswahl des Filters auf neue Datensätze aufgerufen
function refresh(){
    var new_filter = "";
    if ( document.getElementById("filter_list_new").checked==true ) {
        new_filter = "?data=new";
        history.pushState(null, null, window.location.pathname+filter_add("data", "new"));
    } else {
        history.pushState(null, null, window.location.pathname);
    } 
    $.ajax({
            type:'GET',
            url: '/api/produkt_list'+new_filter,
        }).done(function(response){
            //Erfolgreich zurueck gekommen
            if(response != "fail"){
                var html = "<table>";
                for (var i=0; i<response.length;i++){
                    html += "<tr><td class='bt_load_data' onclick='bt_load_data(this)' id='"+response[i]._id+"'>"+response[i].name+" : "+response[i].barcode.code+"</td></tr>";
                }
                document.getElementById('data_table').innerHTML = html + "</table>";
                //count setzen
                document.getElementById('produkt_count').innerHTML = response.length;
                set_msg("Produkt-Liste erfolgreich neu geladen. "+new_filter);
            } else {
                set_msg("Fehler beim Laden der Produkt-Liste.", false);
            }
        });
};

//Funktion zum laden eines Produktes
//wird bei der Auswahl eines Produktes aufgerufen
function bt_load_data(event){
    $.ajax({
            type:'GET',
            url: '/api/'+event.id,
        }).done(function(response){
            //Erfolgreich zurueck gekommen
            if(response != "fail"){
                $.each(response, function(key, value) {
                    if (typeof(value)=="object"){
                        $.each(value, function(key, value) {
                            if (typeof(value)=="object"){
                                 $.each(value, function(key, value) {
                                    document.getElementById(key).value = value;     
                                 });
                            } else {
                               document.getElementById(key).value = value;    
                            }
                        });
                    } else {
                        if (key != "__v" && key != "image"){
                            document.getElementById(key).value = value;   
                        }
                        //gespeichertes Bild laden, falls vorhanden
                        if (key == "image" && value != ""){
                            document.getElementById("image_vorschau").src = value;
                        }
                        //noch kein Bild vorhanden
                        if (key == "image" && value == ""){
                            document.getElementById("image_vorschau").src = "/images/no_pic.jpg";
                        }
                    }
                });
                history.pushState(null, null, '/admin/data/'+response._id+window.location.search);
                set_msg("Produkt: "+response.name+" erfolgreich geladen.");
            } else {
                set_msg("Fehler beim laden des Produkts: "+event.id, false);
            }
        });
    //window.location = "/admin/"+event.id+window.location.search;
}


//Funktion zum speichern eines Produkts
function save(){
    var data = {};
    //Alle input_fenster auslesen
    var all_input =  document.querySelectorAll("#data_fenster .input_produkt");
    for(var i=0; i<all_input.length; i++){
        data[all_input[i].name] = all_input[i].value;
    }
    if (data._id != ""){
        $.ajax({
            type:'PATCH',
            url: '/api/save',
            dataType: 'text',
            data : data
        }).done(function(response){
            if (response == "ok"){
                set_msg("Produkt erfolgreich gespeichert!");    
            } else {
                set_msg("Fehler beim speichern des Produktes", false); 
            }
        });
    } else {
      set_msg("Du hast noch kein Produkt ausgewählt.", false);  
    } 
}

//Funktion zur aktualisierung der Bilder-Vorschau
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function(e) {
      document.getElementById('image_vorschau').src=e.target.result
    }

    reader.readAsDataURL(input.files[0]);
  }
}

//FUnktion zum speichern eines Bildes
function save_img(){
    var file = document.getElementById("image").files[0];
    if (file){
        if (document.getElementById("_id").value != "") {
            var body_data = {
                "_id" : document.getElementById("_id").value
            };
            var myFormData = new FormData();
            myFormData.append('image', file);
            myFormData.append('_id', document.getElementById("_id").value);
            
            //ladebalken einblenden
            document.querySelector(".bar").style.display = "block";
            $.ajax({
                type:'POST',
                url: '/api/image',
                complete: function (response) { 
                    if (response.responseText == "ok"){
                        set_msg("Produkt erfolgreich gespeichert!");    
                    } else {
                        set_msg("Fehler beim speichern des Produktes", false); 
                    }
                    //ladebalken ausblenden
                     document.querySelector(".bar").style.display = "none";
                },
                processData: false, // important
                contentType: false, // important
                dataType : 'json',
                data: myFormData
            });
        } else {
           set_msg("Du hast noch kein Produkt ausgewählt!", false); 
        }
    } else {
        set_msg("Du hast noch kein Bild ausgewählt!", false);
    }
}
//Setzt eine Meldung im Footer
function set_msg(message, type=true){
    var elm = document.getElementById("message")
    var newone = elm.cloneNode(true);
    elm.parentNode.replaceChild(newone, elm);
    document.getElementById('message_txt').innerHTML=message;
    if (type){
        document.getElementById('message_img').style.backgroundImage = "url('/images/message_ok.png')";
    } else {
        document.getElementById('message_img').style.backgroundImage = "url('/images/message_fail.png')";
    }
    document.getElementById('message').style.display = "block";
}