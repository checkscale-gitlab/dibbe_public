//functionen für das einscannen von Produkten als Admin

function start_scan(){
      //start button ausblenden
      document.getElementById("start_scan").style.display = "none";
      document.getElementById("eingabe_panel").style.display = "none";
    document.getElementById("barcode-scanner").style.display = "block";
    if (document.querySelector('#barcode-scanner')){
      Quagga.init({
        inputStream : {
            name : "Live",
            type : "LiveStream",
            constraints: {
                    width: $(document).width(),
                    height: $(document).height(),
                    facingMode: "environment"
            },
            target: document.querySelector('#barcode-scanner')
        },
        locator: {
            patchSize: "medium",
            halfSample: true
        },
        numOfWorkers: 2,
        frequency: 60,
        locate: true,
        decoder : {
          readers : ["ean_reader"]
        }
      }, function(err) {
          if (err) {
              console.log(err);
              return
          }
          console.log("Initialization finished. Ready to start");
          Quagga.start();
      });

      Quagga.onDetected(function(result) {
          //code gefunden
          //scanner stoppen und wieder ausbleben
          Quagga.stop();
          document.querySelector('#ean_code').innerHTML = result.codeResult.code;
          document.querySelector('#barcode-scanner').style.display = "none";
          document.querySelector('#msg').innerHTML = "";
          document.querySelector('#eingabe_panel').style.display = "block";

      });
    } else {
        console.log(navigator.mediaDevice);
        console.log(document.querySelector('#barcode-scanner'));
    }  
}

//produktspeichern
function save(){
    var msg = document.querySelector('#msg');
    if (document.querySelector('#produkt_name').value != ""){
        var data = {
            name : document.querySelector('#produkt_name').value,
            code : document.querySelector('#ean_code').innerHTML,
            typ : "ean"
        }
        $.ajax({
            type:'POST',
            url: '/api/save',
            dataType: 'text',
            data : data
            }).done(function(response){
            if (response == "ok"){
                msg.style.color = "blue";
                msg.innerHTML = "Produkt erfolgreich gespeichert!";   
            } else {
                msg.style.color = "red";
                msg.innerHTML = "Fehler: "+response;
            }
        });   
    } else {
        msg.style.color = "red";
        msg.innerHTML = "Bitte gib einen Produktnamen ein!";   
    }
}

