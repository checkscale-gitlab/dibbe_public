# dibbe

This app was made for an university course with the focus on new concepts of softwaredevelopment.
Implemented for the Duale Hochschule Baden-Württemberg Mannheim.

## Getting Started

### Prerequisites

### Installing

You can either run the application with the provided Vagrantfile or you can run the application on every other system which is running docker.

#### Running with the provided Vagrantfile

#### Running on other Systems

For running the application without the provided Vagrantfile you have to follow the following steps:

1. Clone the repository to your system using:
```
git clone
```
2. Go with the console tool of your choice to the repository folder and run:
```
docker-compose build
```
3. Run the application with:
```
docker-compose up
```
4. Finished:
Now you can visit the application on https://localhost:3000

## Features
During the inital start, there will be two users setup. (One admin one user)
-user@dibbe.com
-test

-admin@dibbe.com
-test

